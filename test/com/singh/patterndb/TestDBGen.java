package com.singh.patterndb;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import org.junit.Test;

public class TestDBGen {

	@Test
	public void testConstructor() {
		DBGen dbGen = new DBGen("1234567x8");
		assertNotNull(dbGen);
	}

	@Test
	public void testConstructorExceptionInvalidArg() {
		try {
			@SuppressWarnings("unused")
			DBGen dbGen = new DBGen("1234567x87");
			fail("Expected IllegalArgumentException");
		} catch(IllegalArgumentException e) {
			assertThat(e.getMessage(), is("Start State must have either 9, 16 or 25 characters"));
		}
	}
	
	@Test
	public void testConstructorExceptionNullArg() {
		try {
			@SuppressWarnings("unused")
			DBGen dbGen = new DBGen(null);
		} catch(IllegalArgumentException e) {
			assertThat(e.getMessage(), is("Start State may not be null"));
		}
	}
	
	@Test
	public void testSlideDown() {
		DBGen dbGen = new DBGen("1234x6758");
		String parent = dbGen.getStartState();
		assertEquals("1234567x8", dbGen.getStateAfterSlideDown(parent, parent.indexOf('x') + 1));
	}
	
	@Test
	public void getStateAfterSlideLeft() {
		DBGen dbGen = new DBGen("12345x786");
		String parent = dbGen.getStartState();
		assertEquals("1234x5786", dbGen.getStateAfterSlideLeft(parent, parent.indexOf('x') + 1));
	}
	
	@Test
	public void getStateAfterSlideRight() {
		DBGen dbGen = new DBGen("123x56478");
		String parent = dbGen.getStartState();
		assertEquals("1235x6478", dbGen.getStateAfterSlideRight(parent, parent.indexOf('x') + 1));
	}
	
	@Test
	public void getStateAfterSlideUp() {
		DBGen dbGen = new DBGen("1234567x8");
		String parent = dbGen.getStartState();
		assertEquals("1234x6758", dbGen.getStateAfterSlideUp(parent, parent.indexOf('x') + 1));
	}
}
