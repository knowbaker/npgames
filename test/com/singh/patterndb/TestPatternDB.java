package com.singh.patterndb;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestBfs.class, TestDBGen.class, TestStates.class })
public class TestPatternDB {

}
