package com.singh.patterndb;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class TestStates {
	public static List<String> one = new ArrayList<String>();
	public static List<String> two = new ArrayList<String>();
	public static List<String> three = new ArrayList<String>();
	
	private String parent;
	private List<String> children;
	
	@BeforeClass
	public static void beforeClass() {
		one.add("1234567x8");
		one.add("12345x786");
		
		two.add("123x56478");
		two.add("1234567x8");
		
		three.add("1x3425786");
		three.add("123x45786");
		three.add("1234857x6");
		three.add("12345x786");
	}
	
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
				{"12345678x", one},
				{"123456x78", two},
				{"1234x5786", three},
		});
	}
	
	public TestStates(String parent, List<String> children) {
		this.parent = parent;
		this.children = children;
	}

	@Test
	public void test() {
		DBGen dbGen = new DBGen("1234567x8");
		assertTrue(compareLists(children, dbGen.getChildren(parent)));
	}
	
	private boolean compareLists(List<String> l, List<String> m) {
		if(l == null || m == null) {
			return false;
		}
		
		if(l.size() != m.size()) {
			return false;
		}
		
		System.out.println(m);
		System.out.println();
		
		for(String e : l) {
			if(!m.contains(e)) {
				return false;
			}
		}
		
		return true;
	}
	
	//TODO
	private static List<String> generatePermuations(String s) {
		if(s == null) {
			throw new IllegalArgumentException("String argument null");
		}
		int l = s.length();
		if(l < 0 || l > Integer.MAX_VALUE) {
			throw new IllegalArgumentException("String too long");
		}
//		List<String> perm = new ArrayList<String>(getNumUniqueChars(s));
		
		return getAllPermutations(s, getNumUniqueChars(s));
	}
	

	//TODO
	private static List<String> getAllPermutations(String s, int numUniqueChars) {
		Set<String> p = new HashSet<String>();
		for(int i = 0; i < s.length(); i++) {
			
		}
		// TODO Auto-generated method stub
		return null;
	}

	private static int getNumUniqueChars(String s) {
		Set<Character> set = new HashSet<Character>();
		for(int i = 0; i < s.length(); ++i) {
			set.add(s.charAt(i));
		}
		return set.size();
	}
}
