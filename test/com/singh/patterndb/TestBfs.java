package com.singh.patterndb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class TestBfs {
	
	private static Map<String, Integer> map;
	private static DBGen dbGen;

	@BeforeClass
	public static void setup() {
		dbGen = new DBGen("12345678x");
		dbGen.bfs();
		map = dbGen.getMap();
	}
	
	@Test
	public void test() {
		assertNotNull(map);
		assertTrue(map.size() == fact(dbGen.getStartState().length())/2);
	}
	
	@Test
	public void testCost() {
		assertEquals(new Integer(4), map.get("x13425786"));
	}

	private int fact(int n) {
		if(n == 0 || n == 1) {
			return 1;
		}
		return n * fact(n - 1);
	}

}
