package com.singh.patterndb;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;

/**
 * Create statememnts like the following:
 * db.pattern.insert({state:"12345678x" cost:0});
 * db.pattern.insert({state:"12345x786" cost:1});
 * db.pattern.insert({state:"1234567x8" cost:1});
 * ...
 * db.pattern.ensureIndex({state:"hashed"});
 * @author psingh
 *
 */
public class DBGen {
	private static int initCost = 0;
	private static String x = "x";
	private String startState = "";
	private Map<String, Integer> map;
	private int len;
	private static enum DIMENSION {
		NINE(9), SIXTEEN(16), TWENTYFIVE(25);
		private int dimension;

		private DIMENSION(int dimension) {
			this.dimension = dimension;
		}
		
		int get(){
			return dimension;
		}
	} 
	class Node {
		String state;
		int cost;
		public Node(String state, int cost) {
			this.state = state;
			this.cost = cost;
		}
	}
	
	public DBGen(String startState) {
		if(startState == null) {
			throw new IllegalArgumentException("Start State may not be null");
		}
		if(DIMENSION.NINE.get() != startState.length() && DIMENSION.SIXTEEN.get() != startState.length() && DIMENSION.TWENTYFIVE.get() != startState.length()) {
			throw new IllegalArgumentException("Start State must have either 9, 16 or 25 characters");
		}
		this.startState = startState;
		this.map = new HashMap<String, Integer>();
		this.len = (int)Math.sqrt(startState.length());
	}

	void bfs() {
		Queue<Node> queue = new ArrayDeque<Node>(); 
		queue.add(new Node(startState, initCost));
		
		//startState is discovered
		map.put(startState, initCost);
		
		while(!queue.isEmpty()) {
			Node e = queue.poll();
			if(e == null) {
				return;
			}

			List<String> children = getChildren(e.state);
			for(String child : children) {
				int newPathCost = e.cost + 1;
				//if child not discovered
				Integer savedCost = map.get(child);
				if(savedCost == null) {
					queue.add(new Node(child, newPathCost));
					//label as discovered
					map.put(child, newPathCost);
				} else {
					//put the lesser of the two costs in the map
					if(newPathCost < savedCost) {
						map.put(child, newPathCost);
					}
				}
			}
		}
	}
	
	List<String> getChildren(String parent) {
		List<String> children = new ArrayList<String>();
		int i = parent.indexOf(x) + 1;
		if(inFirstRow(i)) {//x in first row
			children.add(getStateAfterSlideDown(parent, i));
			if( !(inFirstColumn(i) || inLastColumn(i)) ) {
				children.add(getStateAfterSlideLeft(parent, i));
				children.add(getStateAfterSlideRight(parent, i));
			}
		}
		if(inLastColumn(i)) {//x in last column
			children.add(getStateAfterSlideLeft(parent, i));
			if( !(inFirstRow(i) || inLastRow(i)) ) {
				children.add(getStateAfterSlideUp(parent, i));
				children.add(getStateAfterSlideDown(parent, i));
			}
		}
		if( inFirstColumn(i)) {//x in first column
			children.add(getStateAfterSlideRight(parent, i));
			if( !(inFirstRow(i) || inLastRow(i)) ) {
				children.add(getStateAfterSlideUp(parent, i));
				children.add(getStateAfterSlideDown(parent, i));
			}
		}
		if(inLastRow(i) ) {//x in last row
			children.add(getStateAfterSlideUp(parent, i));
			if( !(inFirstColumn(i) || inLastColumn(i)) ) {
				children.add(getStateAfterSlideLeft(parent, i));
				children.add(getStateAfterSlideRight(parent, i));
			}
		}
		if( !isStrictlyInside(i) ) {
			children.add(getStateAfterSlideDown(parent, i));
			children.add(getStateAfterSlideLeft(parent, i));
			children.add(getStateAfterSlideRight(parent, i));
			children.add(getStateAfterSlideUp(parent, i));
		}
		return children;
	}

	private boolean isStrictlyInside(int i) {
		return inFirstRow(i) || inFirstColumn(i) || inLastRow(i) || inLastColumn(i);
	}

	private boolean inFirstRow(int i) {
		return i <= len;
	}

	private boolean inLastRow(int i) {
		return i > (len * (len - 1));
	}

	private boolean inFirstColumn(int i) {
		return (i - 1) % len == 0;
	}

	private boolean inLastColumn(int i) {
		return i % len == 0;
	}

	String getStateAfterSlideDown(String parent, int i) {
		return parent.substring(0, i-1) + parent.charAt(i - 1 + len) + parent.substring(i, i + len - 1) + x + parent.substring(i + len);
	}
	
	//TODO: Should be able to inject a DB type to generate different insert strings for example for Redis, Mongo etc
	private static String getInsertString(String startState, int cost) {
		return "db.pattern.insert({state:\"" + startState + "\" cost:" + cost + "});";
	}

	public String getStartState() {
		return startState;
	}

	String getStateAfterSlideLeft(String parent, int i) {
		return parent.substring(0, i-2) + x + parent.charAt(i - 2) + parent.substring(i);
	}

	String getStateAfterSlideRight(String parent, int i) {
		return parent.substring(0, i-1) + parent.charAt(i) + x + parent.substring(i+1);
	}

	String getStateAfterSlideUp(String parent, int i) {
		return parent.substring(0, i - 1 - len) + x + parent.substring(i - len, i-1) + parent.charAt(i - 1 - len) + parent.substring(i);
	}

	public Map<String, Integer> getMap() {
		return map;
	}

}
